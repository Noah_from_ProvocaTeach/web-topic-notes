# Web Topic Notes

This is a notebook of various topics loosely related to the Web and Web development. It encompasses the usual HTML, CSS, and JavaScript… but also possibly Julia programming, RDF, OWL, databases, and more. Based on various online resources.