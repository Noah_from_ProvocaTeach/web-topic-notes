# Topics to learn #

## Must know ##
Based loosely on episodes [449](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5mZWVkd3JlbmNoLmNvbS9qcy1qYWJiZXIucnNz/episode/ZGNmNDE3MGItZjAxMy00MmIwLTk2MzgtNzk3Njc2OTg4ODIw), [460](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5mZWVkd3JlbmNoLmNvbS9qcy1qYWJiZXIucnNz/episode/YzkxZTMwNjQtOGVmYy00YTFmLTgzZjgtNGUwNzkxMTk4YWYw), and [471](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy5mZWVkd3JlbmNoLmNvbS9qcy1qYWJiZXIucnNz/episode/MDZhMjEzNjItZTU2My00MGFmLTlkOWQtNDMzMGM0NWNjYjdj) of the JavaScript Jabber podcast.

* Scopes, hoisting, temporal dead zone
* Pure & higher-order functions
* Closures
* Event loops, message queue, microtasks
* Behavior of `this` in JavaScript
* Prototypical inheritance
* Feature detection & polyfills
* Promises, async/await
* Regular expressions
* Method chaining
* JavaScript number/map system

## Should know ##