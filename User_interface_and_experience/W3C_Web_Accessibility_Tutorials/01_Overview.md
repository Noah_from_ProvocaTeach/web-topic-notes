# Overview of the tutorials #

The notes in this directory (`W3C_Web_Accessibility_Tutorials`) and all of its subdirectories cover the [W3C Web Accessibility Tutorials](https://www.w3.org/WAI/tutorials/).

## Outline of tutorials ##

 * [Page Structure](https://www.w3.org/WAI/tutorials/page-structure/)

   * Page Regions

   * Labelling Regions

   * Headings

   * Content Structure

 * [Menus](https://www.w3.org/WAI/tutorials/menus/)

   * Structure

   * Styling

   * Fly-Out Menus

   * Application Menus

 * [Images](https://www.w3.org/WAI/tutorials/images/)

   * Informative Images

   * Decorative Images

   * Functional Images

   * Images of Text
 
 * [Tables](https://www.w3.org/WAI/tutorials/tables/)

   * Tables With One Header

   * Tables With Two Headers

   * Tables With Irregular Headers

   * Tables With Multi-Level Headers

   * Captions \& Summary

 * [Forms](https://www.w3.org/WAI/tutorials/forms/)

   * Labelling Controls

   * Grouping Controls

   * Form Instructions

   * Validating Input

   * User Notifications

   * Multi-Page Forms

   * Custom Controls

 * [Carousels](https://www.w3.org/WAI/tutorials/carousels/)

   * Structure
    
   * Functionality

   * Animations

   * Styling
