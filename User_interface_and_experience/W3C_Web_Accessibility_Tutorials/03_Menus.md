# Menus tutorial #

## Concepts ##

<!----------------------- Section break ------------------------>

## Structure ##

Use semantic markup to indicate menu structure. There are two ways to do this.

### Menu representation ###

#### Approach 1: Unordered list ####

When the menu items are not in a specific order, use an unordered list element `<ul>`.

```html
<ul>
  <li href="/">Home</li>
  <li href="/blog">Blog</li>
  <li href="/shop">Shop</li>
</ul>
```

#### Approach 2: Ordered list ####

When the sequence of menu items is important (e.g. chronological order) use an ordered list element `<ol>`.

```html
<ol>
  <li href="/1935-italy-invades-ethiopia">Italy invades Ethiopia (1935)</li>
  <li href="/1936-39-spanish-civil-war">Spanish Civil War (1936-1939)</li>
  <li href="/1939-40-war-in-europe">War breaks out in Europe (1939-1940)</li>
</ol>
```

### Identify menus ###

Identify the menu with the HTML5 `<nav>` element, so users can access it.

```html
<nav>
  <ul>
    <!-- menu items here -->
  </ul>
</nav>
```

### Label menus ###

Use the `aria-labelledby` or `aria-label` attributes to make the menu easier to find or understand.

```html
<nav aria-labelledby="head-nav">
  <h2 id="head-nav">Navigation</h2>
</nav>
```

### Indicate the current item ###

Let users know where they are in the menu (the current item).

For example, if they are on the "Blog" page, *tell them that,* so they aren't madly clicking the "Blog" link expecting something to happen.

#### Approach 1: Invisible text ####

Add an invisible label, and remove the anchor tag `<a>` so users can't interact with the current item.

HTML:
```html
<li>
  <span class="current">
    <span class="accessLabel">Current page: </span>
    Blog
  </span>
</li>
```

CSS:
```css
.accessLabel { visibility: hidden; }
```

#### Approach 2: WAI-ARIA ####

Use `aria-current="page"` to indicate the current page in the menu. Useful when anchor tag `<a>` can't be removed.

```html
<li>
  <a href="#main" aria-current="page">
    Blog
  </a>
</li>
```

[Accessible Rich Internet Applications (ARIA)](https://www.w3.org/TR/wai-aria-1.2/#introduction) lets developers make *custom* widgets and Web application components accessible to all users, including those with disabilities.

<!----------------------- Section break ------------------------>

## Styling ##

### General considerations ###

#### Images ####

Provide **text alternatives** to images.

#### Location ####

Put the menu where people expect it, e.g. horizontally at the start of the page, or as a sidebar.

#### Identification ####

Make sure users **know it's a menu**. Ways to do this:

  * Headings, e.g. "On this page..."

  * Structural markup (see the [Structure](#structure) section).

  * Color scheme

#### Legibility ####

Make sure **all the text fits**.

Accommodate **varying text sizes**.

**Avoid** line breaks, UPPERCASE TEXT, and hyphenation (these can be distracting).

### Menu items ###

States:

#### Default state ####

#### Hover or focus state ####

Change hovered or focused menu items using the following characteristics:

  * Color

  * Decoration, e.g. <u>underlining</u>

  * Changing shape

#### Active state ####

Indicate when the menu item is tapped or clicked

<!----------------------- Section break ------------------------>

## Fly-out menus ##

<!----------------------- Section break ------------------------>

## Application menus ##
